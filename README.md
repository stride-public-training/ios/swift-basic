# Swift basic training

Corso base linguaggio swift e primo approccio al mondo iOS


Phase 1
------

* Creazione progetto
* Introduzione IBOutlets
* Constraints
* Creazione interfaccia schermate ( Login , list restaurant , detail )
* Navigazione tra le schermate 


Phase 2
------

* Introduzione POD
* Aggiunta librerie necessarie all'app : Alamofire - Firebase - AlamofireSoap
* Gestione chiamate tramite Alamofire 


Phase 3
------

* Invio informazioni
* Notifiche , firebase
* Deep linking